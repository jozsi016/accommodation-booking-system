package hu.jangy.ui;

import hu.jangy.controller.Controller;
import hu.jangy.model.Price;
import hu.jangy.model.Room;

import java.time.LocalDate;
import java.util.Scanner;

public class UserMain {

    private static Controller controller = new Controller();
    public static void main(String[] args){
        System.out.println("Welcome to the Accommodation Booking System");
        System.out.println("Please select from the option below!");
        System.out.println("Option One: List of available  rooms");

        Scanner input = new Scanner(System.in);
        if(input.nextInt() == 1){
            LocalDate start = LocalDate.now();
            LocalDate end = LocalDate.now().plusDays(5);
           for (Room room: controller.getListRoomsByPriceAndDate(Price.CHEAP,start, end)) {
               System.out.println(room);
           }
        }
    }
}
