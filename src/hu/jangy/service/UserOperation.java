package hu.jangy.service;

import hu.jangy.model.User;

public interface UserOperation {
    void operation(User user,UserOperation userOperation );
}
