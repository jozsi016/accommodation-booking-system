package hu.jangy.service;

import hu.jangy.model.Room;
import hu.jangy.model.User;
import java.time.LocalDate;


public class Booking implements UserOperation{


    Room room;
    LocalDate start;
    LocalDate end;


    public Booking(Room room, LocalDate start, LocalDate end){
        this.room = room;
        this.start = start;
        this.end = end;
    }

    @Override
    public void operation(User user, UserOperation userOperation) {
        //this.getRoom().setAvailable(getStart(), getEnd());
        //the room should set to occupied
        user.addBooking(userOperation);
    }

    public void setRoom(Room room) {
        this.room = room;
    }
    public Room getRoom() {
        return room;
    }

    public LocalDate getStart() {
        return start;
    }

    public LocalDate getEnd() {
        return end;
    }
}
