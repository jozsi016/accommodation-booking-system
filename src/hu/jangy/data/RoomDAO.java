package hu.jangy.data;

import hu.jangy.model.Price;
import hu.jangy.model.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomDAO {
    private static List<Room> rooms = new ArrayList<>();

    static {
        for (int i = 0; i <= 100; i++) {
            if (i <= 10) {
                rooms.add(new Room(i, Price.CHEAP, true));
            } else if (i < 10 && i <= 20) {
                rooms.add(new Room(i, Price.MEDIUM, true));
            } else {
                rooms.add(new Room(i, Price.EXPENSIVE, true));
            }
        }
    }

    public static List<Room> getRooms() {
        return rooms;
    }
}

