package hu.jangy.data;

import hu.jangy.model.User;
import hu.jangy.service.Booking;
import hu.jangy.service.UserOperation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookingDAO {
    Map<Long, List<UserOperation>> currentReservation = new HashMap<>();
    Map<Long, List<UserOperation>> pastReservation = new HashMap<>();

    public void addUserToReservations(User user){
        currentReservation.put(user.getUserId(), user.getCurrentReservation());
        pastReservation.put(user.getUserId(), user.getPastReservation());
    }

}
