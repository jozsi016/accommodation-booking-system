package hu.jangy.model;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Room {

    public int roomNumber;
    public boolean available;
    public Price price;
    public Map<LocalDate, Boolean> occupied = new HashMap<>();

    public Room(int roomNumber, Price price, boolean available) {
        this.roomNumber = roomNumber;
        this.price = price;
        this.available = available;
    }

    public boolean isAvailable(LocalDate startDay, LocalDate endDate) {
        if (occupied.isEmpty()) {
            available = true;
        }
        int days = endDate.getDayOfMonth() - startDay.getDayOfMonth();
        for (int i = 0; i <= days; i++) {
            if (occupied.containsKey(startDay.plusDays(i)) && occupied.get(startDay.plusDays(i))) {
                boolean notFree = occupied.get(startDay.plusDays(i));
                return notFree;
            }
        }
        return available;
    }

    public Price getPrice() {
        return price;
    }

    public void setAvailable(LocalDate startDay, LocalDate endDay) {
        int days = endDay.getDayOfMonth() - startDay.getDayOfMonth();
        for (int i = 0; i <= days; i++) {
            occupied.put(startDay.plusDays(i), false);
        }
    }


    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomNumber=" + roomNumber +
                ", available=" + available +
                ", price=" + price +
                '}';
    }
}
