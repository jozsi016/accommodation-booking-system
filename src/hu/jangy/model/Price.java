package hu.jangy.model;

public enum Price {
    CHEAP(10), MEDIUM(20), EXPENSIVE(30);

    private double priceVal;

    Price(double priceVal){
        this.priceVal = priceVal;
    }

    public double getPriceVal() {
        return priceVal;
    }
}
