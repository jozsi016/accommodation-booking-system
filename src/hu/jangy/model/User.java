package hu.jangy.model;

import hu.jangy.service.Booking;
import hu.jangy.service.UserOperation;
import java.util.ArrayList;
import java.util.List;

public class User {
    long userId;
    String name;
    List<UserOperation> currentReservation = new ArrayList<>();
    List<UserOperation> pastReservation = new ArrayList<>();

    public User(long userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addBooking(UserOperation booking) {
        currentReservation.add(booking);
    }

    public void cancellation(UserOperation cancelBooking) {
        currentReservation.remove(cancelBooking);
    }

    public List<UserOperation> getCurrentReservation() {
        return currentReservation;
    }

    public void setCurrentReservation(List<UserOperation> currentReservation) {
        this.currentReservation = currentReservation;
    }

    public List<UserOperation> getPastReservation() {
        return pastReservation;
    }

    public void setPastReservation(List<UserOperation> pastReservation) {
        this.pastReservation = pastReservation;
    }
}
