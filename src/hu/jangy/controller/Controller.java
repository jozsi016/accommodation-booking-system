package hu.jangy.controller;

import hu.jangy.data.RoomDAO;
import hu.jangy.model.Price;
import hu.jangy.model.Room;
import hu.jangy.model.User;
import hu.jangy.service.Booking;
import hu.jangy.service.UserOperation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Controller {

    public List<Room> getListRoomsByPriceAndDate(Price price, LocalDate start, LocalDate end) {
        List<Room> listOfAvailableRooms = new ArrayList<>();
        for (Room room : RoomDAO.getRooms()) {
            if (room.isAvailable(start, end) && room.getPrice() == price) {
                listOfAvailableRooms.add(room);
            }
        }
        return listOfAvailableRooms;
    }

    public void createBooking(User user, Room room, LocalDate start, LocalDate end) {
        if (room.isAvailable(start, end)) {
           Booking booking =  new Booking(room, start,end);
           user.addBooking(booking);
        } else {
            System.out.println("booking was failed!");
        }
    }

    public void cancelBooking(User user, UserOperation cancellation) {
        user.cancellation(cancellation);
    }
}


